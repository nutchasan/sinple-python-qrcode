# Simple-python-qrcode



## Getting started

Install qrcode and pillow.

```
pip install -r requirements.txt.
```

## Set your string

In 'create_qrcode.py', Replace your string on 'Your String'.

```
if __name__ == '__main__':
    create_qrcode('Your String')
```

## Run the code

Run code with command line.

```
python create_qrcode.py
```
then your qrcode png file will generate on current directory.
