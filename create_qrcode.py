import qrcode 

def create_qrcode(text):
    print('Creating qrcode')
    qr = qrcode.QRCode()
    qr.add_data(text)
    qr.make(fit=True)
    qrcode_img = qr.make_image()
    # qrcode_img.show()
    qrcode_img.save('qrcode_image.png')
    print('Success to create qrcode')

if __name__ == '__main__':
    create_qrcode('Your String')
